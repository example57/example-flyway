package com.example.exampleflyway.config;

import javax.sql.DataSource;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    @Autowired
    private DataSource dataSource;

    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }

//    @Bean(initMethod = "migrate")
//    @DependsOn("springUtility")
//    public Flyway flyway() {
//        Flyway flyway = new Flyway();
//        flyway.setBaselineOnMigrate(true);
//        flyway.setDataSource(dataSource);
//        return flyway;
//    }
}
