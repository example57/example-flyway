package com.example.exampleflyway.controller;

import com.example.exampleflyway.api.AccountService;
import com.example.exampleflyway.dto.AccountDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping("/save")
    public AccountDTO saveAccount(@RequestBody @Validated AccountDTO accountDTO) {
        return accountService.save(accountDTO);
    }
}
